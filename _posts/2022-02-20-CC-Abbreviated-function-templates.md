---
layout: post
title: "الگوهای مخفف تابع در ++C"
author: "پریسا خالقی"
tags: tutorial cpp cpp20 function-templates
excerpt_separator: <!--more-->
comments: true
sticky: false
hidden: false
---

تعریف کردن function templateها در ++C همیشه پیچیده و طولانی و در عین حال عجیب و جذاب بوده.
حالا در C++20 استاندارد روش جدیدی برای تعریف function templateها معرفی کرده که مختصرتر و سازگارتر با Lambdaها است به نام: **Abbreviated function templates**
یا همان الگوهای مخفف تابع.
در این پست می‌خواهیم نحوه استفاده از این function templateها به‌همراه نحوه اعمال آن‌ها در C++20 concepts ها را بررسی کنیم.

در C++11 لامبداها معرفی شدند که به این شکل بودند:
{% highlight cpp  %}
[captures] (type_1 param_1, type_2 param_2) { body(param_1, param_2); }
{% endhighlight  %}
شما فقط می‌توانید این لامبدا را با آرگومان‌های `type_1` و `type_2` فراخوانی کنید.
با این‌حال، ما اغلب از لامبدا در موقعیت‌هایی استفاده می‌کنیم که املای کاملِ انواع آن دشوار است (مخصوصا موقع استفاده از ویژگی‌هایی مثل `ranges`).

حالا C++14 به شما این امکان را می‌داد که لامبداهایی بسازید که با استفاده از کلمه‌کلیدی `auto` می‌توانید با هر نوع آرگومانی آن را فراخوانی کنید:
{% highlight cpp  %}
[captures] (auto param_1, auto param_2) { body(param_1, param_2); }
{% endhighlight  %}
اکنون می‌توانید هر Typeای را به عنوان آرگومان ارسال کنید. Abbreviated function templateها در C++20 به شما این امکان را می‌دهد که این نوع syntax را در function templateها اعمال کنید.

در C++17 ممکن است بخواهیم تابعی بنویسیم که به‌عنوان یک Function template، حیوانات را سرشماری کند، بنابراین می‌توانیم کاری کنیم آن تابع با هر نوع حیوانی فراخوانی شود:
{% highlight cpp  %}
template <class Animal>
void animal_counts(Animal const& the_animal);
{% endhighlight  %}

در C++20 می‌توانید این کار را به‌سادگی با `auto` انجام دهید:
{% highlight cpp  %}
void animal_counts(auto const& the_animal);
{% endhighlight  %}

این نسخه کوتاه‌تر است، نیاز به نام‌گذاری‌های کمتری دارد و با C++14 Lambdas سازگارتر است.

با این‌حال، الگوی تابع بالا مشکلی دارد: طبق تعریف، ما می‌توانیم به معنای واقعی کلمه، هرچیزی را به فانکشن پاس بدهیم، در واقع ما می‌توانیم فراخوانی‌هایی به این شکل داشته باشیم:
{% highlight cpp  %}
animal_counts(42);
animal_counts(a_flower);
animal_counts(a_computer);
animal_counts(the_platonic_ideal_of_a_chair);
{% endhighlight  %}
این‌ها ممکن است کامپایل شوند و کاری انجام دهند یا ممکن است به دلیل اجرای templateئه کاری که Typeها از آن پشتیبانی نمی‌کنند، موفق به کامپایل نشوند.

## Constrained Auto:
در حالت ایده‌آل، ما هم خدا را می‌خواهیم و هم خرما را! یعنی این‌که هم می‌خواهیم رابط این الگوی تابع را با Typeهایی که پشتیبانی می‌شود مستند کنیم و هم به کامپایلر این امکان را بدهیم که وقتی declaration ما با type ناسازگاری نمونه‌سازی می‌شود، خطاهای دقیق بدهد.
اینجاست که C++20 به ما Conceptها را می‌دهد تا برای حل این مشکل به ما کمک کند.
اگر `animal concept`ای داشته باشیم که مشخص می‌کند یک type که یک `Animal` را نشان می‌دهد چه interfaceای باید داشته باشد، می‌توانیم از آن به این‌صورت استفاده کنیم:
{% highlight cpp  %}
template <animal Animal>
void animal_counts(Animal const& the_animal);
{% endhighlight  %}
خب این کار کاملا کوتاه و البته تکراری است. در حالت ایده‌آل، می‌توانیم از نام `concept` به‌صورت مستقیم در function parameter list استفاده کنیم:
{% highlight cpp  %}
void animal_counts(animal const& the_animal);
{% endhighlight  %}
 با این حال، این نحو از استانداردسازی رد شد، چون نمی‌توانید تشخیص دهید که آیا این یک function template است یا یک فانکشن معمولی، بدون اینکه بدانید `animal` یک type است یا یک `concept`.

خوشبختانه نسخه‌ای از این نحو در C++20 قرار گرفت که دوباره از کلمه‌کلیدی `auto` استفاده می‌کند:
{% highlight cpp  %}
void animal_counts(animal auto const& the_animal);
{% endhighlight  %}

این بررسی می‌کند که هرچیزی که جایگزین auto شود، خواسته‌ی `animal concept` را برآورده می‌کند. بنابراین اگر `template` را با یک بچه‌گربه instantiate کنیم، :
{% highlight cpp  %}
animal<kitten>
{% endhighlight  %}
بررسی می‌شود. این کار، یک سینتکس مختصر را به ما باز‌می‌گرداند و در عین حال به ما اجازه می‌دهد تا declarationهای template خودمان را محدود کنیم. هم خدا را برآورده کردیم و هم خرما را!
