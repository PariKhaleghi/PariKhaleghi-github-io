---
layout: post
title: "منابع پیشنهادی برای یادگیری ++C"
author: "پریسا خالقی"
tags: resources cpp
excerpt_separator: <!--more-->
comments: false
sticky: false
hidden: false
---
![C++ Resources](/images/1_O2bRvRN4ZZSF3tNvr8zfKA.jpeg "منابع یادگیری سی‌پلاس‌پلاس")

- دوره فارسی خوب کی می‌شناسه برای ++C؟
- یه منبع صفر تا صد معرفی کنید.
- با چه کتابی / فیلمی / دوره‌ای / شروع کنم به یادگیری این زبون؟
- چه سایتی خوبه؟
- من هنوز توی دانشگاه به این مبحث نرسیدم، می‌شه از این تابع استفاده نکنید؟
- و...

این‌ها تماما سؤالاتی هستند که ما هرروز حداقل یک‌بار در مجامع عمومی مثل گروه‌های تلگرامی یا سرکار یا هرجای دیگری با آن‌ها مواجه می‌شویم.
مشکل اساسا از آن‌جایی شروع می‌شود که تعداد منابع آموزشی برای یادگیری یک چیز سر به فلک می‌کشد.
هرکس شیوه تدریس خود را دارد، هرکس آن طور که دلش می‌خواهد به یک موضوع می‌پردازد، برخی افراد از سادگیِ مردم سوءاستفاده کرده و با وعده‌وعیدهای دروغین، آنها را تشویق به خرید دوره یا کتاب‌شان می‌کنند.

برخی‌ها به دنبال راحت‌طلبیِ خودشان هستند و زحمت مطالعه منابع خوب و زبان اصلی را به خود نمی‌دهند، برخی دیگر نیز خامِ تبلیغاتِ سایت‌های عوام‌فریب شده و سرمایه خود را برای یادگیری یک زبان برنامه‌نویسی به فنا می‌دهند و دست آخر تنها چیزی که نصیب‌شان می‌شود، تمام Bad Practice ها و البته یک `using namespace std` است!
و از طرفی باید قبول کرد دانشگاه مکان خوبی برای یادگیری برنامه‌نویسی نیست. دانشگاه نهایتا بتواند شما را یک هولِ ریز بدهد تا موتور شما روشن شود. اما باید توجه داشته باشید که این موتور، نیاز به سوخت بیشتری برای حرکت دارد، و سوخت این موتور تنها زمانی تأمین می‌شود که خود شما برای به‌دست آوردن آن تلاش کنید! ++C زبان بسیار گسترده و بزرگی‌است و یادگیری اصولی آن از نان شب واجبتر است!

ما در این پست قصد داریم به معرفی منابع تأیید شده و اصولی بپردازیم تا حتی‌الامکان جهانی با برنامه‌نویسانِ خوب و با سطح دانش و اطلاعات بالا، دور هم گرد آوریم. بنابراین تا حد امکان سعی کرده‌ایم که از معرفی منابع فارسی‌زبان بپرهیزیم. خب دیگر مقدمه بس است. به معرفی منابع بپردازیم.

---
# مبانی
از شروع برنامه‌نویسی به زبان ++C شروع می‌کنیم.
مطمئناً قبول دارید که شروع هرچیزی، مهم‌ترین بخش آن چیز است. اینکه ما به چه شکل و به چه نحوی یادگیری را شروع بکنیم، فوق‌العاده به مسیری که در آینده پیش‌رو داریم، کمک می‌کند. ++C زبانی است که به‌شدت در حال رشد و ارتقا است و نحوه یادگیری آن وابسته به روز، ممکن است تغییر کند. بنابراین ما به منبعی نیاز داریم که بتواند با مباحث جدید و البته از سطح پایه یادگیری این زبان را برای ما آسان کند.
## LearnCpp
![LearnCpp](/images/Screenshot%202022-03-13%20113344.png "LearnCpp")

سایت [learncpp.com](https://learncpp.com) به اصلِ سادگی معتقد است و تمام مطالب را از پایه‌ای‌ترین حد ممکن شروع می‌کند و ذهن شما را برای پذیرفتن مطالب پیشرفته‌تر آماده می‌کند.

این سایت تمام مطالب را با استانداردهای جدید تطبیق داده و به‌روز نگه‌می‌دارد. همچنین این سایت با داشتن Best Practiceهای مبتدیانه در هر بخش، شما را از Bad Practiceهایی که در دانشگاه و جاهای دیگر یاد می‌گیرید، دور نگه می‌دارد.

هم‌اکنون تمامی مطالب این سایت بر روی استاندارد 11 تا 20 قرار گرفته‌اند.

## A Tour of C++
![A Tour of C++](/images/817c6KcRr2L.jpg "A Tour of C++")

در کنار سایت [learncpp.com](https://learncpp.com)، می‌توانید با استفاده از یک جزوه، دانش خود را درباره قابلیت‌های مهم موجود در زبان ++C ارتقا دهید. کتاب [++A Tour of C](https://www.amazon.com/Tour-2nd-Depth-Bjarne-Stroustrup/dp/0134997832) حاصل تدریس خود جناب بیارنه استراستروپ است (سازنده و توسعه‌دهنده زبان ++C) و دارای نثر بسیار روان است که در حین خواندن کتاب خسته و کلافه نشوید. تمام مطالب به‌طور خلاصه بیان شده‌اند؛ پس می‌توانید با مطالعهٔ این کتاب یک دید کلّی به قابلیّت‌های زبان سی‌پلاس‌پلاس داشته باشید.

## The C++ Programming Language
![The C++ Programming Language](/images/81b-3FJTE9L.jpg "The C++ Programming Language")

در ادامه برای آشنایی کامل با استاندارد 11 می‌توانید کتاب [The C++ Programming Language](https://www.amazon.com/C-Programming-Language-4th/dp/0321563840) را مطالعه نمایید.
این کتاب نیز همانند کتاب قبلی توسط خالق ++C، جناب بیارنه استراستروپ نگارش شده‌است.

زبان ++C از استاندارد 11 دچار تغییرات بسیار گسترده و وسیعی شد که این زبان را کاملا از C-Style بودن دور کرد و تبدیل به یک زبان کاملا مدرن و جدید کرد. به همین علت، استراستروپ به طور کامل تغییراتی را که باعث می‌شود زبان C++11 مانند یک زبان کاملاً جدید احساس شود، بررسی می‌کند و راهنمایی قطعی برای بهبود عملکرد آن ارائه می‌دهد.

کتاب The C++ Programming Language یک کتاب آموزش زبان سی‌پلاس‌پلاس نیست، این کتاب تمام قابلیّت‌ها و فلسفه‌های طراحی سی‌پلاس‌پلاس-استاندارد یازده را مورد بررسی قرار داده؛ پس از این کتاب به عنوان یک مرجع استفاده نمایید.

---

# متوسط
حالا وقت آن است که بیشتر وارد لایه‌های این زبان شویم!

## Effective Modern C++

 ![Effective Modern C++](/images/71eTsvokmjL.jpg "Effective Modern C++")

تسلط به C++11 و C++14 بیش از آشنایی با ویژگی‌هایی است که آنها معرفی می‌کنند (به عنوان مثال، اعلان‌های نوع `auto`، معناشناسی move، عبارات لامبدا و پشتیبانی concurrency). صرفا یادگیری این‌ها مهم نیست. مهم این است که یاد بگیرید از آن ویژگی‌ها به طور موثر استفاده کنید — به طوری که نرم‌افزار شما صحیح، کارآمد، قابل نگهداری و قابل حمل باشد. اینجاست که کتاب [++Effective Modern C](https://www.amazon.com/Effective-Modern-Specific-Ways-Improve/dp/1491903996) کاربردی وارد می‌شود و نحوه نوشتن یک نرم‌افزار عالی با استفاده از C++11 و C++14 را شرح می‌دهد.

موضوعات مطرح‌شده در این کتاب عبارتند از: مزایا و معایب braced initialization، مشخصات `noexcept`، فوروارد کامل و  smart pointer، روابط بین `std::move`، `std::forward` ارجاعات rvalue، و تکنیک‌هایی برای نوشتن لامبداهای واضح، صحیح و موثر. چگونگی متفاوت بودنِ `std::atomic` با `volatile`، و اینکه چگونه باید از هر کدام استفاده کرد، و چگونه باconcurrency API در ++C ارتباط دارند. و کلی مطالب حیاتی دیگر. این کتاب، مهمترین کتاب برای مشاوره در مورد دستورالعمل‌های کلیدی، استایل‌ها و اصطلاحات برای استفاده مدرن است.

## Beautiful C++: 30 Core Guidelines for Writing Clean, Safe, and Fast Code
![Beautiful C++](/images/1642858091237.jpg "Beautiful C++")

پس از اتمام ++Effective Modern C دچار سردرگمی‌ها و پیچیدگی‌های بسیاری درباره این زبان می‌شوید که مطمئنا جمع‌آوری آنها در کنار یکدیگر و استفاده از آن‌ها با پیروی از تمام قوانین و نادیده‌گرفتنِ کابوس‌هایی که از پیچیدگی این زبان در ذهن شما رخ می‌دهد، دشوار است.

برای نوشتن کد تمیز، سریع و عالیِ ++C نیازی به سختی نیست. دستورالعمل‌های اصلی ++C می‌تواند به هر توسعه‌دهنده ++Cای کمک کند تا برنامه‌هایی را طراحی و بنویسد که فوق‌العاده قابل اعتماد، کارآمد و با عملکرد خوب هستند. اما دستورالعمل‌ها در این زبان آنقدر مملو از توصیه‌های عالی هستند که سخت است بدانید از کجا شروع کنید.
از این‌جا: کتاب [++Beautiful C](https://www.amazon.com/Beautiful-Core-Guidelines-Writing-Clean/dp/0137647840) که اخیرا (در سال 2021) توسط گای دیویدسون و کیت گرگوری نوشته و منتشر شده‌است، دستورالعمل‌های اصلی و کاربردی که برای همه برنامه‌نویسان ++C ارزش ویژه‌ای دارند و دانش عملی مفصلی برای بهبود سبک ++C نویسی شما ارائه می‌دهند را در برمی‌گیرد. برای ارجاع آسان، این کتاب به گونه‌ای طراحی شده است که با وب‌سایت رسمی C++ Core Guidelines هماهنگ باشد،

هدف این کتاب این چهار مورد است:
- با نوشتن کدی که بعداً مشکل ایجاد می کند، خود را آزار ندهید.
- از کدام ویژگی‌های قدیمی اجتناب کنید و به جای آن‌ها از ویژگی‌های مدرن استفاده کنید.
- از ویژگی‌های جدیدتر به درستی استفاده کنید تا بدون ایجاد مشکلات جدید از مزایای آنها بهره‌مند شوید.
- کد پیش‌فرضی بنویسید با کیفیت بالاتر که به‌صورت استاتیک Type-safe است، مقاوم در برابر نشتی است و تکامل آن آسان‌تر است.

---

# پیشرفته
تا اینجا شما تازه با ساختار و مفاهیم پایهٔ زبان آشنا شده‌اید، حال نیاز به عمیق شدن در بخش‌های مختلف زبان است.


## C++ Concurrency In Action - Second Edition
![C++ Concurrency In Action](/images/Williams-CC-HI.png "C++ Concurrency In Action")

کتاب [C++ Concurrency in Action](https://www.amazon.com/C-Concurrency-Action-Anthony-Williams/dp/1617294691) توسط آقای آنتونی ویلیامز، یکی از برنامه‌نویسان Boost.Thread، نوشته شده است که به‌صورت کاملاً مبتدی تا حرفه‌ای به توضیح و بررسی برنامه‌نویسی موازی و نحوهٔ استفاده از Threading و انواع روش‌های مهم Syncronization در زبان سی‌پلاس‌پلاس، می‌پردازد؛ که هر برنامه‌نویس سی‌پلاس‌پلاس باید اشراف کامل بر این مطالب را داشته باشد..

## C++ Templates: The Complete Guide - Second Edition
![C++ Templates](/images/ShowCover.jpg "C++ Templates")
پس از مطالعهٔ LearnCpp، شما با مفاهیم ابتدایی Templateها در زبان سی‌پلاس‌پلاس، آشنا شده‌اید، حال نیاز است که به دقّت آن‌ها را مورد بررسی قرار بدید. این کتاب، در بخش Basics تمام مفاهیم مورد نیاز یک برنامه‌نویسی سی‌پلاس‌پلاس، در مورد Templateها را توضیح داده و در بخش‌های بعدی، به بررسی تک‌تک جزئیات Templateها می‌پردازد.

در زبان سی‌پلاس‌پلاس، Template‌ها یکی از قدرتمندترین ویژگی‌ها هستند، اما حتی با پیشرفت زبان ++C و جامعه توسعه‌دهنده، از آنها سوءتفاهم و استفاده نادرست باقی مانده‌است. در [C++ Templates: The Complete Guide](https://www.amazon.com/C-Templates-Complete-Guide-2nd/dp/0321714121) سه کارشناس پیشگام ++C نشان می‌دهند که چرا، چه زمانی و چگونه از قالب‌های مدرن برای ساختن نرم‌افزاری تمیزتر، سریع‌تر، کارآمدتر و نگهداری آسان‌تر استفاده کنیم.

این نسخه جدید که اکنون به طور گسترده برای استانداردهای C++11، C++14 و C++17 به روز شده است، تکنیک‌های پیشرفته‌تری را برای طیف وسیع‌تری از برنامه‌ها ارائه می‌کند. نویسندگان توضیحات معتبری درباره همه ویژگی‌های زبان جدید ارائه می‌دهند که یا الگوها را بهبود می‌بخشند یا با آنها تعامل دارند، از جمله Templateهای متغیر، لامبداهای Generic، کسر class template argument و if در زمان کامپایل، ارجاعات forwarding، و literalهای تعریف‌شده توسط کاربر. این کتاب همچنین عمیقاً در مفاهیم اساسی زبان (مانند مقوله‌های value) کاوش می‌کند و تمام ویژگی‌های نوع استاندارد را به طور کامل پوشش می‌دهد.

---

# حرفه‌ای
در این قسمت، ما به بررسی زبان از دید/زبان ریاضی و منطقی می‌پردازیم.

## Element Of Programming
![Element Of Programming](/images/Elements%20of%20Programming.jpg "Element Of Programming")

کتاب [Element Of Programming](http://elementsofprogramming.com/) به بررسی و مطالعهٔ الگوریتم‌ها و مفاهیم پایهٔ سیستم (Entities, Value, Type , ...) از یک دید انتزاعی می‌پردازد؛ که به ذهن شما کمک شایانی در نحوهٔ فکر کردن به طراحی الگوریتم‌ها و برخورد با داده‌ها می‌کند.

## From Mathematics To Generic Programming
![From Mathematics To Generic Programming](/images/from-mathematics-to-generic-programming-1.jpg "From Mathematics To Generic Programming")

کتاب [From Mathematics To Generic Programming](https://www.amazon.com/Mathematics-Generic-Programming-Alexander-Stepanov/dp/0321942043) در واقع به نوعی می‌تواند گفت که دنبالهٔ کتاب Element Of Programming می‌باشد که به‌صورت عمیق‌تری به طراحی با اثبات ریاضیات می‌پردازد.

## Modern C++ Design
![Modern C++ Design](/images/914ncVx1hxL.jpg "Modern C++ Design")

بعد از اینکه با نحوهٔ صحیح استفاده از Templateها در کتاب C++ Templates: The Complete Guide آشنا شدید، نیاز است که دانش بیشتری دربارهٔ نحوهٔ صحیح طراحی ساختار برنامه برای همگام‌سازی با Templateها، کسب کنید. کتاب [Modern C++ Design](https://www.amazon.com/Modern-Design-Generic-Programming-Patterns/dp/0201704315) یک کتاب قدیمی می‌باشد که روندهای جالبی برای استفاده از Templateها در طراحی ساختار برنامه‌ها را توضیح داده است.

---

# منابع جانبی
## CppReferences
سایت [CppReferences](http://cppreference.com/) یک سایت فوق‌العاده عالی و به‌روز برای استفاده به عنوان مرجع زبان می‌باشد. در این سایت، تک‌تک قابلیّت‌های زبان و کتابخانهٔ استاندارد زبان، توضیح و مستند شده است که نیاز همیشگی یک برنامه‌نویس سی‌پلاس‌پلاس می‌باشد. می‌توانید از قسمت [Archives](https://en.cppreference.com/w/Cppreference:Archives) نسخهٔ Offline را دانلود کنید.

## CppCon | The C++ Conference
زبان سی‌پلاس‌پلاس، عملاً یکی از پیرمردان عرصهٔ خود هست. پس بعید نیست که جامعهٔ بزرگی هم به همراه خود داشته باشد. [CppCon](https://cppcon.org/) سال‌هاست که مکانی برای کنفرانس‌های بسیار مهم سی‌پلاس‌پلاس است. پیشنهاد می‌کنم که حتماً ارائه‌های سری Back To Basics را در Youtube تماشا بکنید که برای افراد مبتدی، توضیحات بسیار خوبی دارند.



**این پست برای پرداختن به سطوح پیشرفته و سایر فریمورک‌ها و کتابخانه‌های پرکاربرد (از جمله Boost و Qt) به‌روزرسانی خواهد شد.**
