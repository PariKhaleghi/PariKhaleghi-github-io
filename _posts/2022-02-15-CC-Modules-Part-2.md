---
layout: post
title: "سی‌پلاس‌پلاس۲۰ و ماژول‌ها - قسمت دوّم"
author: "پریسا خالقی"
tags: tutorial cpp cpp20 modules
excerpt_separator: <!--more-->
comments: true
sticky: false
hidden: false
---

در C++20، به هر فایلی که حاوی سینتکس ماژول باشد، Module Unit گفته می‌شود. بنابراین یک ماژول نام‌گذاری شده ممکن است از یک یا چند Module Unit تشکیل شده باشد.

# ماژول تک فایلی (کامل)
## بخش اول: کد Pre-C++20
بیایید با Hello World شروع کنیم. خب از تقسیم رفتار در دو فایل آغاز می‌کنیم:
ما دو فایل func.cpp و main.cpp داریم:

- فایل **func.cpp**
{% highlight cpp  %}
#include <iostream>

void func() {  // definition
    std::cout << "Hello, World!\n";
}
{% endhighlight %}

- و فایل **main.cpp**
{% highlight cpp  %}
void func();  // declaration

int main() {
    func();
}
{% endhighlight %}

ما می‌توانیم این برنامه را build و run کنیم:
{% highlight bash  %}
$ g++ -c func.cpp
$ g++ -c main.cpp
$ g++ -o App main.o func.o
$ ./App
Hello, World!
{% endhighlight %}

در این برنامه چون تابع `func`، به‌طور پیش‌فرض External Linkage دارد، با موفقیت Build می‌شود.
بنابراین تا زمانی که `main` یک declaration معتبر داشته باشد، فایل `main.cpp` می‌تواند کامپایل شود و linker سمبل‌های imported / exported را مدیریت کند.

## بخش دوم: ماژول C++20:
در پروپوزال P1103R3 بود که پیشنهادات اولیه برای پشتیبانی از ماژول‌ها به این شکل صادر شد:

{% highlight bash  %}
A complete module can be defined in a single source file.
{% endhighlight %}

ابتدا ما باید ماژول نام‌گذاری شده‌ی خودمان را ایجاد کنیم. یک فایل ماژول کامل، معمولا دارای دو، یا احتمالا سه بخش (به نام fragments) خواهد بود:

1. یک global module fragment - جایی‌ست که چیزهایی را که نیاز داریم اضافه می‌کنیم (اختیاری).
2. محدوده اصلی ماژول - جایی‌ست که می‌توانیم typeها و رفتارها را export کنیم.
3. یک fragment اختصاصی - این بخش به قسمت اینترفیس ماژول که می‌تواند بر رفتارِ سایر translation unitها تاثیر بگذارد، پایان می‌دهد. (اختیاری).

> نکته: fragment خصوصی فقط می‌تواند در ماژول‌های تک‌فایلی ظاهر شود. نسخه فعلی GCC یعنی 11.2 از fragmentهای private پشتیبانی نمی‌کند. اما ما از آن‌ها در این پست، چشم‌پوشی نمی‌کنیم.

استاندارد ++C پسوند فایل را تعریف نمی‌کند. این صرفا یک toolchain specific است. در GCC، پسوند نام فایل، نحوه برخورد با فایل را برای هر فایل ورودی مشخصی تعیین می‌کند. GCC پسوند‌های فایل زیر را به عنوان Source کد ++C تفسیر می‌کند که باید از قبل پردازش شوند:
{% highlight bash  %}
file.cc
file.cp
file.cxx
file.cpp
file.c++
file.C
{% endhighlight %}
ما همیشه پسوند `cpp.` را برای فایل‌های سورس ++C در پروژه‌هایمان و هنگام آموزش ++C ترجیح می‌دهیم. بنابراین برای این منظور، در مثال‌های زیر از `cpp.` برای فایل‌های سورس معمولی ++C، و از `cxx` برای فایل‌های ماژول استفاده خواهم کرد. این صرفا یک ترجیح شخصی‌است.
قابل ذکر است، مایکروسافت استفاده از پسوند `.ixx` را برای رابط‌های ماژول انتخاب کرده است. ما می‌توانیم از `file.ixx` استفاده کنیم، اما در GCC، باید از این دستور:
{% highlight bash  %}
-x c++ file.ixx
{% endhighlight %}
 استفاده کنیم تا مشخص کنیم که فایل باید به‌عنوان یک فایل ++C در نظر گرفته شود. به جای اینکه لقمه را دور سرمان بچرخانیم، از همان `cxx.` استفاده می‌کنیم، به این معنی که GCC آن را به عنوان یک فایل ++C استاندارد در نظر بگیرد.

برای تبدیل فایل اصلی `func.cpp` به یک ماژول (`func.cxx`)، این خط را اضافه می‌کنیم:
{% highlight cpp  %}
export module MODULE-NAME;
{% endhighlight  %}
برای مثال:

- فایل **func.cxx**

{% highlight cpp  %}
#include <iostream>

export module mod;

void func() {
    std::cout << "hello, world!\n";
}
{% endhighlight  %}
با این حال، این کد کامپایل نخواهد شد. عبارت `include` باید در Global Fragment باشد. Global Fragment باید مقدم بر main purview باشد و به‌سادگی با کلمه‌کلیدی `module` معرفی شود، مثال:

- فایل **func.cxx**

{% highlight cpp  %}
module;

#include <iostream>

export module mod;

void func() {
    std::cout << "hello, world!\n";
}
{% endhighlight  %}
حالا می‌توانیم این ماژول را `main` اضافه کنیم:

- فایل **main.cpp**

{% highlight cpp  %}
import mod;

int main(){
    func();
}
{% endhighlight  %}
حالا می‌توانیم `func.cxx` را کامپایل کنیم:
{% highlight bash  %}
$ g++ -c -std=c++20 -fmodules-ts func.cxx
{% endhighlight  %}
توجه داشته باشید، در GCC C++20، ماژول‌ها در حال حاضر تنها با مشخص کردن C++20 فعال نمی‌شوند. شما همچنین باید دستورالعمل `-fmodules-ts` را بنویسید.

خب، همانطور که انتظار می‌رود، کامپایلر یک آبجکت فایلِ `func.o` ایجاد می‌کند. با این حال، همچنین متوجه خواهید شد که یک زیرشاخه به نام `gcm.cache` با فایل `mod.gcm` ایجاد شده است. این فایل رابط ماژول تولید شده توسط کامپایلر است.

اگر پیش برویم و `main.cpp` را کامپایل کنیم:
{% highlight bash  %}
$ g++ -c -std=c++20 -fmodules-ts  main.cpp
{% endhighlight  %}

{% highlight bash  %}
> main.cpp: In function 'int main()':
main.cpp:5:5: error: 'func' was not declared in this scope
    5 |     func();
      |     ^~~~
{% endhighlight  %}
ما این خطا را دریافت می‌کنیم که `func` تعریف نشده است. اگر بخواهیم آن را در `main.cpp` اعلام کنیم (مانند قبل) ساخته می‌شود اما در link شدن fail خواهد شد.
    
بنابراین، این اولین تغییر مهم را به ما می‌دهد:
    
**در ماژول ها، تمام اعلان‌ها و تعاریف، private هستند مگر اینکه export شوند.**

اما صبر کنید، دیگر Linker فقط یک Linker داخلی نیست! حالا به‌طور رسمی با Module Linker مواجهیم و کاملا با Linker داخلی متفاوت است. این تفاوت زمانی آشکار می‌شود از یک ماژول و پارتیشن‌های چندفایلی استفاده کنید (بعدا در پستی جداگانه به این موضوع خواهیم پرداخت.)
برای رفع این مشکل، تابع را `export` می‌کنیم:
    
- فایل **func.cxx**
{% highlight cpp  %}
module;

#include <iostream>

export module mod;

export void func() {
    std::cout << "hello, world!\n";
}
{% endhighlight  %}
    
اکنون پروژه با موفقیت کامپایل و Link می‌شود:
{% highlight bash  %}
$ g++ -c -std=c++20 -fmodules-ts func.cxx
$ g++ -c -std=c++20 -fmodules-ts main.cpp 
$ g++ main.o func.o -o App
$ ./App 
> hello, world!
{% endhighlight  %}
    
## جداسازی فایل‌های اینترفیس و پیاده‌سازی:
در برخی مواقع، ممکن است ماژول خود را به چندین فایل تقسیم کنیم تا بتوانیم آن را مدیریت کنیم و به زمانِ rebuild شدنِ پروژه کمک کنیم.

همانطور که گفتیم، هر فایل مربوط به یک ماژول، Module Unit نامیده می شود. ما دو unit ایجاد می‌کنیم:
    
* یک واحد رابط ماژول اولیه یا همان PMIU (Primary Module Interface Unit).
* یک واحد پیاده‌سازی ماژول

### Primary Module Interface Unit:
هر ماژول نام‌گذاری شده تنها و تنها باید یک واحد رابط ماژول اولیه داشته باشد. این فایل اصلاح شده **func.cxx** است که حاوی این عبارت است:
{% highlight cpp  %}
export module MODULE-NAME;
{% endhighlight  %}
مثال:
{% highlight cpp  %}
export module mod;

export void func();
{% endhighlight  %}
    
این تمام چیزی است که ما نیاز داریم؛ ما یک ماژول را نام‌گذاری کرده‌ایم و حالا می‌توانیم مثل گذشته آن را کامپایل کنیم.

### Module Implementation Unit:
در حال حاضر هیچ تبدیل نام‌گذاری اصطلاحی‌ای وجود ندارد، بنابراین من با **func_impl.cxx** پیش می‌روم. شما نمی توانید از **func.ixx** استفاده کنید زیرا یک آبجکت فایل **func.o** را نیز ایجاد می‌کند که آبجکت فایل ایجاد شده توسط **func.cxx** را بازنویسی می‌کند.
یک واحد پیاده‌سازی شامل خط زیر است:
{% highlight cpp  %}
module MODULE-NAME;
{% endhighlight  %}
    
این امر به طور ضمنی هر چیزی را که در PMIU تعریف شده را در بخش پیاده‌سازی در دسترس قرار می‌دهد (برعکس این موضوع صادق نیست). توجه داشته باشید که بخش‌های پیاده‌سازی نمی‌توانند export داشته باشند.
- فایل **func_impl.cxx**
{% highlight cpp  %}
module;

#include <iostream>

module mod;

void func() {
    std::cout << "hello, world!\n";
}
{% endhighlight  %}
حالا می‌توانیم این واحد پیاده‌سازی را کامپایل کنیم:

{% highlight bash  %}
$ g++ -c -std=c++20 -fmodules-ts func_impl.cxx
{% endhighlight  %}
که func_impl.o را ایجاد می‌کند - ما دو آبجکت فایل ماژول را به عنوان بخشی از linkage خود داریم، البته، این بدان معناست که تغییرات در پیاده‌سازی ماژول نیازی به کامپایل مجدد از سوی کلاینت‌ها ندارد (تقلید از رفتار هدرهای موجود).
{% highlight bash  %}
$ g++ main.o func.o func_impl.o -o App
$ ./App 
> hello, world!
{% endhighlight  %}

با GCC، بخش اینترفیس باید قبل از بخش پیاده‌سازی کامپایل شود.

## Export:
تا کنون، ما فقط یک تابع را export کرده‌ایم. با فرض اینکه چندین تابع برای export داریم، استاندارد با استفاده از کلمه‌کلیدی export این امکان را به ما می‌دهد.

- فایل **func.cxx**
{% highlight cpp  %}
export module mod;

export void func();
export void func(int);
{% endhighlight  %}

## بلوک Export:
از طرف دیگر، به‌سادگی می‌توانیم بسیاری از declarationها را در یک بلوک `export` گروه‌بندی کنیم، مثال:

{% highlight cpp  %}
export module mod;

export {
    void func();
    void func(int);
}
{% endhighlight  %}
در بخش بعدی به فضای نام‌ها و export کردن آن‌ها در ماژول‌ها خواهیم پرداخت.
