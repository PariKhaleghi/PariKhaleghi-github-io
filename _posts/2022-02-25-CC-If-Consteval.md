---
layout: post
title: "if consteval"
author: "پریسا خالقی"
tags: tutorial cpp cpp23
excerpt_separator: <!--more-->
comments: true
sticky: false
hidden: false
---

همانطور که در بخش قبلی گفتیم، کمیته استاندارد شکل جدیدی از دستور `if` را پیش‌نویس کرده که سعی در حل مشکلات `std::is_constant_evaluated` و `consteval` دارد:
{% highlight cpp  %}
if consteval { }
{% endhighlight  %}

 اگر ارزیابی این عبارت در حین constant evaluation اتفاق بیافتد، اولین substatement اجرا می‌شود. در غیر این‌صورت، substatement دوم (در صورت وجود) اجرا می‌شود.
در واقع `if consteval` دقیقا مثل:
{% highlight cpp  %}
if (std::is_constant_evaluated()) { }
{% endhighlight  %}
رفتار می‌کند، با سه تفاوت:
* دیگر نیازی به header include نیست.
* سینتکس متفاوتی دارد، که به‌‌طور کامل ما را از سردرگمی در مورد روش مناسب برای بررسی اینکه آیا در constant evaluation هستیم یا خیر، دور می‌کند.
* می‌توانیم از `if consteval` برای فراخوانی immediate functionها استفاده کنیم.
برای توضیحِ بیشترِ نکته آخر: قوانین فعلی، به شما اجازه می‌دهد که یک تابع `consteval` را از داخل تابع `consteval` دیگر فراخوانی کنید. ما می‌توانیم این کار را با این construction انجام دهیم:
> An expression or conversion is in an immediate function context if it is potentially evaluated and its innermost non-block scope is a function parameter scope of an immediate function. An expression or conversion is an immediate invocation if it is an explicit or implicit invocation of an immediate function and is not in an immediate function context. An immediate invocation shall be a constant expression.

منبع: [[expr.const]/13](http://eel.is/c++draft/expr.const#13)

 با توسعه‌دادنِ immediate function context برای شامل‌شدنِ بلوک if consteval، می‌توانیم به همان مثال قبلی‌مان ([مثال پست قبل](https://parikhaleghi.github.io/2022-02-21/CC-Consteval#:~:text=constexpr%20int%20g(int%20i))) اجازه کار بدهیم:
 {% highlight cpp  %}
consteval int f(int i) { return i; }

constexpr int g(int i) {
    if consteval {
        return f(i) + 1; // ok: immediate function context
    } 
    else {
        return 42;
    }
}

consteval int h(int i) {
    return f(i) + 1; // ok: immediate function context
}
{% endhighlight  %}

علاوه بر این، چنین ویژگی‌ای، امکان پیاده‌سازی آسان `std::is_constant_evaluated` را فراهم می‌کند:
{% highlight cpp  %}
constexpr bool is_constant_evaluated() {
    if consteval {
        return true;
    }
    else {
        return false;
    }
}
{% endhighlight  %}
به همین سادگی!

## شکل نفی:
بسیاری از مردم این نظر را بیان کرده‌اند که شکل نفی `if consteval` نیز مفید است. این شکل را نیز کمیته به این شکل پیشنهاد داده:
{% highlight cpp  %}
if not consteval { }
{% endhighlight  %}
یا
{% highlight cpp  %}
if ! consteval { }
{% endhighlight  %}
یعنی اگر contextما constant evaluated نبود، substatement اول اجرا شود و در غیر این‌صورت، substatement دوم اجرا شود.

## شکل مشروط:
همان‌طور که گفتیم، این شکل جدید از if، یک شرط ندارد! برخلاف دو شکل قبلی (`if` و `if constexpr`). در حالی که مواردی وجود دارد که یک شرط اضافه می‌تواند مفید باشه. کمیته اقرار می‌کند که تصمیم ندارد فعلا شکل شرطی را شامل شود چون باور دارد اکثر استفاده‌ها فقط به شکلِ `if consteval` یا `if not consteval` باشد و نمی‌خواهد فضای طراحی آینده را در این زمینه شلوغ کند.

در حال حاضر دو استفاده در ++libstdc وجود دارد که به این شکل هستند:
{% highlight cpp  %}
if (is_constant_evaluated() && cond)
{% endhighlight  %}
مثال:
{% highlight cpp  %}
if (std::is_constant_evaluated() && __n < 0)
  throw "attempt to decrement a non-bidirectional iterator";
{% endhighlight  %}

 یا می توان آن را به صورت زیر نوشت:
{% highlight cpp  %}
if consteval {
  if (__n < 0) throw "attempt to decrement a non-bidirectional iterator";
}
{% endhighlight  %}
یا:
{% highlight cpp  %}
if consteval {
  consteval_assert(__n >= 0,
    "attempt to decrement a non-bidirectional iterator");
}
{% endhighlight  %}
در هر صورت، کمیته انگیزه‌ای برای مشروط‌سازیِ `if consteval` ندارد.

## منسوخ کردن `std::is_constant_evaluated`:
یکی از سوالاتی که مطرح می‌شود این است: اگر `if consteval` داشته باشیم، دیگر چه نیازی به `std::is_constant_evaluated` داریم؟ آیا کمیته آن را منسوخ می‌کند؟
پاسخ کمیته این است که فعلا نقشه‌ای برای منسوخ‌سازی `is_constant_evaluated` ندارد. دلیلش این است که این تابع در واقع هنوز هم گاهی اوقات مفید است (همانطور که در چندخط قبلی نوشتیم)، در واقع اگر کمیته آن‌را حذف کند، کاربران میایند و فانکشن خودشان را برای این‌کار می‌نویسند. کمیته نگران دشواریِ نوشتنِ آن نیست، کاربرانی که به این فانکشن نیاز داشته باشند قطعا می‌توانند آن را به درستی بنویسند، اما نگران  گسترس دقیق این عملکرد هستند.
یک مزیت دیگرِ داشتنِ `std::is_constant_evaluated` این است که قابل آموزش است و هم اینکه هشدار‌پذیر (Warnable) است.
و توجه داشته باشید که ++libstdc کاربردهای دیگری دارد که به فرمِ تابع نیاز دارند.

## چرا پرانتز؟
پرانتزها در `if consteval` اجباری هستند:
{% highlight cpp  %}
if consteval {
   f(i); // ok
}

if consteval f(i); // ill-formed
{% endhighlight  %}

هیچ دلیل فنی‌ای برای اجباری بودنِ پرانتز وجود ندارد. دلیل کمیته برای این موضوع صرفا این است که برنامه‌نویس به‌صورت بصری حواسش باشد که در حالِ رفتن به یک زمینه کاملا متفاوت است.
همچنین کاملاً متفاوت از `constexpr` است که قوانین زبان در `if consteval` واقعاً تغییر می‌کند، بنابراین کمیته فکر می‌کند که شایسته قوانین مختلف است.

بدون پرانتز، ما هیچ جدایی بین معرفی‌کننده‌ی `if consteval` و دستورِ در حال اجرا نداریم. این امر در صورتی مهم می‌شود که دستور اجرا شده خود شامل نوعی کلمه‌کلیدی باشد:
بدون پرانتز، ما هیچ جدایی بین معرفی کننده `if consteval` و دستور در حال اجرا نداریم. این امر به ویژه در صورتی مهم می‌شود که دستور اجرا شده خود شامل نوعی کلمه‌کلیدی باشد:
{% highlight cpp  %}
if consteval for (; it != end; ++it) ;
if consteval if (it != end);
{% endhighlight  %}

یک دستور if معمولی دارای یک شرط پرانتز است که به عنوان یک جداکننده عمل می‌کند، و کمیته فکر می‌کند چنین جداسازی بصری‌ای مهم است:
{% highlight cpp  %}
if (cond) f(i);
{% endhighlight  %}
یک سینتکس جایگزین پیشنهاد شده این بود:
{% highlight cpp  %}
if (consteval)
{% endhighlight  %}
بدون نیاز به پرانتز، که در این صورت بیشتر شبیه یک دستور `if` معمولی بود. اما این سینتکس نشان می‌دهد که اَشکال دیگری ممکن است معتبر باشند، که کمیته نمی‌تواند از آن‌ها به دلیل heroicهای پیاده‌سازیِ لازم برای این کار پشتیبانی کند:
{% highlight cpp  %}
if (consteval && n < 0) {
  // We would need to take an arbitrary boolean condition, C, and
  // be able to prove that (not is_constant_evaluated) implies (not C).
  // That's an arbitrarily complex problem.
  some_consteval_fun(n);
}
{% endhighlight  %}
 
یا نشان می‌دهد که اَشکال دیگری ممکن است معتبر باشند که حتی استفاده از آنها، منطقی نیست:
{% highlight cpp  %}
while (consteval) { /* ... */ }
switch (consteval) { /* ... */ }
{% endhighlight  %}

به طور کلی، کمیته فکر می‌کند این واقعیت که اگر `consteval` با یک دستور `if` معمولی متفاوت به‌نظر برسد، مسلماً یک مزیت است.

## مثال
در اینجا چند نمونه از ++libstdc آورده شده است. پیاده‌سازی با استفاده از یک تابع builtin، و تبدیل آنها به`consteval`. تفاوت زیادی را مشاهده نمی‌کنید، صرفا spelling عوض شده است:

### پیاده‌سازی `libstdc++`
{% highlight cpp  %}
if (!__builtin_is_constant_evaluated())
  __glibcxx_assert( __shift_exponent != numeric_limits<_Tp>::digits );
{% endhighlight  %}

{% highlight cpp  %}
if (__builtin_is_constant_evaluated())
  return __x < __y;
return (__UINTPTR_TYPE__)__x > (__UINTPTR_TYPE__)__y;
{% endhighlight  %}

### پیاده‌سازی با `if consteval`
{% highlight cpp  %}
if not consteval {
   __glibcxx_assert( __shift_exponent != numeric_limits<_Tp>::digits );
}
{% endhighlight  %}

{% highlight cpp  %}
if consteval {
  return __x < __y;
} else {
  return (__UINTPTR_TYPE__)__x > (__UINTPTR_TYPE__)__y;
}
{% endhighlight  %}
