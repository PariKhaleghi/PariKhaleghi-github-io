---
layout: post
title: "مشکلات consteval و std::is_constant_evaluated"
author: "پریسا خالقی"
tags: tutorial cpp cpp20
excerpt_separator: <!--more-->
comments: false
sticky: false
hidden: false
---

‏C++20 چندین ویژگی جدید برای کمک به برنامه‌نویسان در پرداختن به  constant evaluationها داشت. دو مورد از آنها `std::is_constant_evaluated` و `consteval` هستند، که هر دو در 2018 به تصویب رسیدند. `consteval` برای توابعی است که فقط در طول ارزیابی `constant` می‌توانند فراخوانی شوند. `is_constant_evaluated` یک تابع جادویی برای بررسی اینکه آیا evaluationئه فعلی constant evaluation است یا خیر استفاده می‌شود تا برای مثال، پیاده‌سازی معتبری از یک الگوریتم برای زمان constant evaluation و اجرای بهتر برای Run-time ارائه دهد.

با این حال، این ویژگی ها تعامل ضعیفی با یکدیگر دارند و مسائل دیگری دارند که توسعه‌دهندگان را سردرگم می‌کند که در ادامه به آن‌ها خواهیم پرداخت.

**نکته: ما در این مقاله قصد نداریم شما را با `consteval` و `std::is_constant_evaluated` آشنا کنیم، بلکه تنها به مشکلات آن‌ها خواهیم پرداخت.
برای آشنایی اولیه با این موارد، از گوگل کمک بگیرید.**

## دو مشکل وجود دارد که این مقاله می‌خواهد به آن بپردازد:
### 1. تعامل بین `constexpr` و `consteval`:

اولین مشکل تعامل بین آن تابع جادویی و `consteval` جدید است. به مثال توجه کنید:
{% highlight cpp  %}
constexpr int g(int i) {
    if (std::is_constant_evaluated()) {
        return f(i) + 1; // <==
    } else {
        return 42;
    }
}

consteval int h(int i) {
    return f(i) + 1;
}
{% endhighlight  %}
تابع `h` در اینجا اساساً یک نسخه  lift شده و با ارزیابی ثابت از تابع `g` است. در زمانِ ارزیابی ثابت هم همین کار را می‌کنند، با این تفاوت که در Run-time نمی‌توانید `h` را فراخوانی کنید و `g` این مسیر اضافی را دارد. شاید این کد فقط با `h` شروع شده باشد و شخصی تصمیم بگیرد که یک نسخه از رانتایم را مفید بداند و آن را به `g` تبدیل کند.

متأسفانه `h` خوب شکل گرفته در حالی که `g` بد فرم است. شما نمی‌توانید آن فراخوانی را با `f` (که در کد با یک فلش مشخص شده است) در آن مکان برقرار کنید. حتی اگر این فراخوانی تنها در طول constant evaluation اتفاق بیافتد، کافی و به‌دردبخور نیست.

با یکسری شرایط خاص، فراخوانی `f` در داخل `g` یک فراخوانی Immediate است و باید یک constant expression باشد و اینطور نیست (توضیح: در C++20، یک تابع Immediate تابعی است که در آن هر فراخوانی تابعی به‌طور مستقیم یا غیرمستقیم یک compile-time constant expression تولید می‌کند.)
این توابع (immediate) با استفاده از یک کلمه کلیدی `consteval` قبل از نوع برگشتی خود اعلام می شوند. در حالی که فراخوانی `f` در داخل `h` یک immediate invocation در نظر گرفته نمی‌شود زیرا در یک context از تابع immediate قرار دارد (یعنی از یک تابع immediate دیگر فراخوانی شده است)، بنابراین دارای مجموعه ضعیف‌تری از محدودیت‌ها است که باید از آنها پیروی کند.

به عبارت دیگر، این نوع ساختارِ فراخوانیِ شرطیِ تابعِ `consteval` از یک تابع `constexpr` کار نمی‌کند.

ما این عدم ترکیب‌پذیری ویژگی‌ها را مشکل‌ساز می دانیم و فکر می‌کنیم که می‌توان آن را بهبود بخشید.

### 2. مشکل `if constexpr(std::is_constant_evaluated())` :
مشکل دوم مختص `std::is_constant_evaluated` است. کاربرد واضح آن این است:
{% highlight cpp  %}
constexpr size_t strlen(char const* s) {
    if constexpr (std::is_constant_evaluated()) {
        for (const char* p = s; ; ++p) {
            if (*p == '\0') {
                return static_cast<std::size_t>(p - s);
            }
        }    
    } 
    else {
        asm("SSE 4.2 insanity");        
    }
}
{% endhighlight  %}

این مثال یک Bug دارد: از `if constexpr` برای بررسی شرطی `is_constant_evaluated` به جای `if` ساده استفاده می‌کند. شما باید عمیقاً چیزهای زیادی در مورد نحوه عملکرد constant evaluation در ++C درک کنید تا بفهمید که این مثال در واقع نه تنها صحیح نیست، بلکه از بیخ غلط است. GCC 11.2  یک Warn برای این مثال می‌دهد:
{% highlight bash  %}
<source>: In function 'constexpr int f(int)':
<source>:4:45: warning: 'std::is_constant_evaluated' always evaluates to true in 'if constexpr' [-Wtautological-compare]
    4 |     if constexpr (std::is_constant_evaluated()) {
      |                   ~~~~~~^~

{% endhighlight  %}
اما پس از آن مردم باید بفهمند که چرا این Warn را می‌دهد، و این حتی به چه معناست. با این وجود، هشدار دادنِ کامپایلر به طور قابل توجهی بهتر از کدهای اشتباهِ بی‌صدا است، اما داشتن یک API که در آن بسیاری از کاربران به سمت استفاده‌ از چیزی کشیده می‌شوند که از لحاظ توتولوژیکی نادرست است، مشکل‌ساز است.

### هشدارهای کامپایلر:
مدت‌ها پیش، پیاده‌کنندگان کامپایلرها اطمینان دادند که همه کامپایلرها به درستی در مورد همه استفاده‌های توتولوژیک از `std::is_constant_evaluated` هشدار می‌دهند - هم در موارد همیشه `true` و هم در موارد همیشه `false`.

به عنوان مثال، در زمان نوشتن این پست، EDG در مورد همه موارد زیر Warning می‌دهد: 
{% highlight cpp  %}
constexpr int f1() {
  if constexpr (!std::is_constant_evaluated() && sizeof(int) == 4) { // warning: always true
    return 0;
  }
  if (std::is_constant_evaluated()) {
    return 42;
  } else {
    if constexpr (std::is_constant_evaluated()) { // warning: always true
      return 0;
    }
  }
  return 7;
}


consteval int f2() {
  if (std::is_constant_evaluated() && f1()) { // warning: always true
    return 42;
  }
  return 7;
}


int f3() {
  if (std::is_constant_evaluated() && f1()) { // warning: always false
    return 42;
  }
  return 7;
}
{% endhighlight  %}
ما انتظار داریم که سایر کامپایلرها نیز همین روند را دنبال کنند.


**و حالا کمیته شکل جدیدی از دستور `if` را پیش‌نویس می‌کند که سعی در حل این مشکلات دارد:**
`if consteval`

**در بخش بعدی به‌ `if consteval` خواهیم پرداخت.**
