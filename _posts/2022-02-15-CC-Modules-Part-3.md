---
layout: post
title: "سی‌پلاس‌پلاس۲۰ و ماژول‌ها - قسمت سوّم"
author: "پریسا خالقی"
tags: tutorial cpp cpp20 modules
excerpt_separator: <!--more-->
comments: false
sticky: false
hidden: false
---

## فضای نام:
همانطور که قبلاً ذکر شد، فضاهای نام ++C متعامد با ماژول‌ها هستند. اعتراف می‌کنم که این در ابتدا باعث سردرگمی من شد. نه به اندازه نحو، فلسفه عمومی استفاده از فضاهای نام و ماژول ها، جایی که هر کدام در یک ساختار معماری قرار می‌گیرند می‌تواند گیج‌کننده باشد.

از منظر عملی، فضاهای نام مانند قبل رفتار می‌کنند، بنابراین هیچ تغییر واقعی در استفاده از آنها وجود ندارد، به عنوان مثال:
- فایل **func.cxx**
{% highlight cpp  %}
export module mod;

namespace X {
    export void func();
    export void func(int);
}
{% endhighlight %}
- فایل **func_impl.cxx**
{% highlight cpp  %}
module;

#include <iostream>

module mod;

namespace X {
    void func() {
        std::cout << "hello, world!\n";
    }

    void func(int p) {
        std::cout << "hello, " << p << '\n';
    }
}
{% endhighlight %}
- فایل **main.cpp**
{% highlight cpp  %}
import mod;

int main(){
    X::func();
    X::func(42);
}
{% endhighlight %}
## ‏Export کردنِ فضای نام:
اگر فضای نامی را Export کنیم، تمام declarationهای داخل آن فضای نام به‌طور خودکار در اینترفیسِ ماژول گنجانده می‌شوند، به‌عنوان مثال:
- فایل **func.cxx**
{% highlight cpp  %}
export module mod;

export namespace X {
    void func();
    void func(int);
}
{% endhighlight %}

## Export کردنِ Typeها و...:
هرچیزی که به‌عنوان بخشی از اینترفیس ماژول نیاز داریم، باید Export شود. به‌عنوان مثال، اگر تابع یک object reference را به‌عنوان پارامتر بگیرد، قوانین type definition visibility معمولی بر روی آن اعمال می‌شود. مثال را در Gist ببینید.


 - https://gist.github.com/PariKhaleghi/d0fbd0b582c5cce1a3df8b75eb4ed1b8


مجموعه ای از قوانین و Exceptionها در مورد Export کردنِ آیتم‌ها، مانند templateها و ارزیابی ADL وجود دارد. من قصد ندارم وارد این موارد در اینجا شوم زیرا بسیار use-specific است. شاید بعدا در پستی دیگر، به‌صورت اختصاصی به این موضوع پرداختیم.

## Includeها:
در مثال بالا، می‌بینید که از دستورالعمل پیش‌پردازنده‌ی سنتیِ `include#` برای گنجاندن هدر کتابخانه استاندارد `iostream` استفاده کرده‌ایم. استاندارد موارد زیر را مجاز می‌کند:
{% highlight cpp  %}
import <iostream>;
import "header.h";
{% endhighlight %}
این توسط GCC11 پشتیبانی می‌شود، اما برخی فیلترها وجود دارد که باید از آنها عبور کنید تا ابتدا یک هدر تعریف‌شده توسط کاربر قابل Export باشد (به دستورالعملِ `-fmodule-header` مراجعه کنید).

اگر یک فایل header-only دارید، مثل:
- فایل **header.h**
{% highlight cpp  %}
#ifndef _HEADER_
#define _HEADER_

constexpr int life = 42;

#endif
{% endhighlight %}
و می‌خواهید آن‌را import کنید باید اول آن را کامپایل کنید:
{% highlight bash  %}
$ g++ -c -std=c++20 -fmodule-header header.h
{% endhighlight %}
این یک فایلِ `header.h.gcm` تولید می‌کند. اکنون می‌توانید هدر را `import` کنید:
{% highlight cpp  %}
import "header.h";
 {% endhighlight %}
به Semicolon دقت کنید!

به‌علاوه، مایکروسافت قبلا کتابخانه استاندارد را در یک ساختار ماژول قرار داده است، بنابراین در MSVC می‌توانید از std.core استفاده کنید.
import std.core

در پست‌های بعدی، در مطالبی پیچیده‌تر، تواناییِ تقسیم اینترفیس یک ماژول به چندین فایل (به نام پارتیشن) را پوشش خواهیم داد. با استفاده از پارتیشن‌ها، سطح ظاهری ساده به نظر می‌رسد، اما این صرفا یک خیال واهی است! 😄

با ما همراه باشید.
